# Stockholm UI Manager Module

## Introduction

Stockholm module can be used to create UI elements, or any animated component in the same category, easily and (ideally) in an error free manner. Naturally, no other core module is expected to be dependent to this module, and it is understandable developers have their own way of doing things, but we strongly recommend creation and management of Game User Interface using Stockholm UI Manager.

## How to Install

* Import Stockholm proxy module from Package Manager UI.

* It has some dependencies to some other utility modules, they will be automatically loaded with this module.

* Odin Inspector has to be installed in Unity.

  That's it.

## The Structure

To use Stockholm module, we need a singleton UI Manager which references a `Stockholm` component in initial scene. In basic terms, this component knows how to load (mount) a certain UI prefab then destroy (unmount) it when we are done with it. You can find more information about the contents of `Stockholm.cs` in later sections.

In each UI unit, we are imitating a MVC framework. In this framework, we are managing the UI using `Controller` components associated with each `View` and `Model`.

![MVC](/img~/MVC.png)

Let's call a complete UI unit a `User Control`.  

* This user control has to have a `Controller` component so that we can load, open, close and finally destroy it with methods set in it. 

* The `View` component is needed if we want to animate the User Control when it is opening and closing, or simply there is any visual in UI.
* And `Model` can be used to seperate the gameplay parameters from the controller script, so that it is much more organized. For example this way localization can be handled much more easily.

We are ideally trying to provide complete components ready and easy to implement for a structured development, and it works best when these three components work together. Every user control unit needs to have exactly one Controller component, and ideally one View component and a Model class.
Additionally single User Control can have lots of sub-views controlled by this parent controller. Since View component is a script that be used to animate a certain UI item, it is actually expected to be attached to sub-UI items without a controller attached to those same items.

## How to Create an Animated Popup (UserControl)

To understand the process better assume that we are trying to create an Animated Win Popup that shows us the followings;
* A "Congratulations" Text that fades in.
* An Animated Panel that contains the coin information we win in that level.
* An Animated Close Button that is shown after all other animations completed.
* Inanimate Title Text.

![CreateUIFinal](/img~/CreateUIFinal.png)

First, right click on any Canvas in Hierarchy Window, creation of UI elements is only possible under Canvas hierarchy.  Now, go to `UI > MG Stockholm` and in GameObject menu and click on `User Control`. This will open a `Create User Control` Menu.

![CreateUserControl](/img~/CreateUserControlMenu.png)

In this menu, you have two options;
* You can create a new User Control with your custom Controller script
* Or, you can create a User Control with one of previously created Controller scripts.

For now let's assume we want to create our own script, to do that click on `Create Controller` toggle. This will enable additional settings:

![CreateControllerSettings](/img~/CreateControllerSettings.png)

As you can see Controller, View and Model are all script components which will be created as described in case they are selected. For name, we can put in anything prefarably in Pascal-Case. These scripts are created in predestined locations, and later they are searched in these locations, so it is best to not move them later. These predestined paths can be edited in configuration, please refer to [] for more information. For now let's put `Win/Win` for name, this means we want to create `WinController.cs`, `WinView.cs` and `WinModel.cs` scripts inside `Win` directories.

Now, since we want our Popup Animated, we are going to need an animated view.  In `View Templates` dropdown, you can select one of the templates to be used easily. 

* **Scaled (Linear) :** Animation changing the scale of UI element from Vector2.zero to Vector2.one linearly in a given duration.
* **Scaled (AnimationCurve) :** Animation changing the scale of UI element according to the data in AnimationCurve component attached in a given duration.
* **Faded (Linear) :** Animation changing the opacity of UI (Image) element from Vector2.zero to Vector2.one linearly in a given duration.
* **Faded (AnimationCurve) :** Animation changing the opacity of UI (Image) element according to the data in AnimationCurve component attached in a given duration.
* **Animator :** Uses Animation component attached to the UI element.
* **Empty :** Empty View. This just enables and disables the UI. i.e. no animation.

We select Animator from templates.

When we click `Create Scripts` it creates the scripts and selects our controller script for UI creation process. In case there is already a script with the same name in any of the paths, there is additional popup which informs if you would like to overwrite them. We advise not to overwrite anything if you are not sure.

After scripts are created you should see the `ControllerScript` dropdown, `ViewTypeName` and `ModelTypeName` filled. Here, there can be a little bug with updating ViewTypeName and ModelTypeName. To solve that select the WinController from the dropdown again. And View and Model types associated with our controller should be fetched.

![CreateScriptsFinal](/img~/CreateScriptsFinal.png)


Now we can create our WinController UI clicking the button below. There are two additional settings here:

* **Add Input Lock :** This is a front overlay which can be used to block access to our user control when animation is in place for example.
* **Add Bg Lock :** Bg Lock on the other hand is a background overlay which can be used to block access to what is behind of our User Control. 

Ideally both of them should be selected if it is a popup, but for inanimate user control like ObjectiveBar, you may choose to leave them deactivated.

Now click the button and we can see that `WinController` UserControl has been created under Canvas.

## How to Create Subviews

The structure of a certain User Control comprises of a pivotal GameObject at the top which contains Controller and View components, and a child root which contains everything except InputLock (LockOverlay) and BgLock (BgOverlay). In other words, we have to put sub UI element under root object.

Let's recall what we had to do from previous chapter, and create them one by one.

**A "Congratulations" Text that fades in:**

* Right Click on Root object.
* Select `UI > MG Stockholm > Text` from the menu. `Create View Text Element` menu should open.
* Here, we can actually create a new View script to be overriden by ourselves in selected directory. But, for simple animations like fading in, we can use predefined templates which is under `View Script Dropdown`.
* Select `FadedView.cs` under `Packages>com.matchighamgames.StockholmCore>Runtime>Templates>View`. 
* Click `Create UI`. The `FadedText` field should be created.
* Change the name to WinText and inside Text Component, write "Congratulations". 

![CreateTextView](/img~/CreateTextView.png)

**An Animated Panel that contains the coin information we win in that level:**

* Right Click on Root object.
* Select `UI > MG Stockholm > Panel` from the menu. `Create View Panel Element` menu should open.
* Select `AnimatedView` under `Packages>com.matchighamgames.StockholmCore>Runtime>Templates>View`. 
* Click `Create UI`. The `AnimatedPanel` field should be created.
* Change the name to RewardPanel. 
* We are not done yet because we want this panel to contain another text of how much coin we have. To do that, just add a regular Text UI. Since it is not going to be animated, we do not need View component here.
* Name the Text UI as "CurrentGoldText".

**An Animated Close Button that is shown after all other animations completed:**

* Right Click on Root object.
* Select `UI > MG Stockholm > Button` from the menu. `Create View Button Element` menu should open.
* Select `AnimatedView` under `Packages>com.matchighamgames.StockholmCore>Runtime>Templates>View`. 
* Click `Create UI`. The `AnimatedButton` field should be created.
* Change the name to CloseButton. 

That is it. We are almost done. The last element in our TODO list is `Inanimate Title Text`. Again, we do not need any animation for this element, so regular Text UI should do the trick.

After arranging their sizes and places, it becomes something like this:

![CreateUIFinal](/img~/CreateUIFinal.png)

Observe that everything except the overlays are under Root gameobject. If you take a look at the Inspector window, under Win View you will see the fields under `Base Component` header. WinView is an inherited class which manages the popup animation as a base functionality. Depending on the View template you choose, you will see different type of settings here. And they come with predefined parameters and references to assets which are also created if there is a need.

So far we just created WinController . To be able to load and instantiate it in our game we are going to need a prefab. For that, you can just need to;

* Right click on WinController in Hierarchy
* Click `UI > MG Stockholm > Create as Prefab` 

The WinController is now created in predefined path as a prefab. This location is under Resources folder to be able to load it conveniently. After WinController in hierarchy turned blue indicating it is a reference to a prefab, you can safely delete it from the hierarchy.

## Stockholm API Details

As we mentioned in the beginning, we load and unload usercontrols with the help of a singleton component called **Stockholm**. To utilize it, in initial scene; 

* First create a UIManager GameObject.
* Add Stockholm component on it.
* And finally create a `UIManager.cs` script and add it as well.

In Stockholm component, there is only one field `Default Root`. This is the default location the User Controls would be instantiated on. If we have only one scene in our game, it is preferably best to assign a dedicated canvas for User Controls and reference this canvas in here.

The functionality of Stockholm can be grouped under these two main functions;

### Loading and Instantiating UserControls

``` csharp
Mount <TController>():
```

This is used to Load and Instantiate the referenced Controller.

Example Usage:

``` csharp
WinController win = Stockholm.Instance.Mount<WinController>(string key = "");	
// Loads the selected Controller and instantiates it on default canvas given on.
// key: a tag to group controllers
// throws MissingReferenceException if no default root is assigned.
```

``` csharp
Transform t = ...
WinController win = Stockholm.Instance.Mount<WinController>(Transform t, string key); 
// Loads the selected Controller and instantiates it on given transform t, logically a Canvas.
// key: a tag to group controllers
// Alternatively use: 
WinController win = t.Mount<TestController>();
```

Observe that;

* You can use different canvases to instantiate your UserControls.
* You can group these controllers under certain group tag (key) to be referenced conveniently.
* If no key is given, default key is empty string, and you can still reference them with this key.
* If there are more than one scenes in your game, you may not be able to use default canvas for it will be destroyed in scene change.

### Unloading UserControls

**Unmount:** Destroys the instantiated controller and gameobject.

Example Usage:

``` csharp
WinController win = ...
Stockholm.Instance.Unmount(win);
// Removes the preloaded controller instance, destroying it.
```

``` csharp
Stockholm.Instance.Unmount(string key);
// Removes and destroys all controller instances grouped with a certain key.
// Alternatively use: 
WinController win = ...
win.Unmount();
```

``` csharp
Stockholm.Instance.Unmount<TController>();
// Removes and destroys all controller instances of a given controller type
```

Observe that;

* You can destroy certain controller and its gameobject through unmounting, even if it is not created with Stockholm.

* You can destroy all controller instances with key or type, if there is any created under that key or type.

* To unmount everything, you can just use;

  ``` csharp
  Stockholm.Instance.Unmount<Controller>();
  ```

  since they are all derived from Controller class.

### Utility Functions

- TryGetWithKey
  
    ```csharp
    bool TryGetWithKey(string key, out List<Controller> result);
    ```

    Tries to find all controllers grouped under certain group key.

---

- TryGetWithType

    ```csharp
    bool TryGetWithType<TController>(out List<TController> result) where TController : Controller;
    ```

    Tries to find all controllers of certain controller type

---

- IsAny

    ```csharp 
    bool IsAny();
    ```

    Checks if there is any controller

---

- IsAny(string

    ```csharp
    bool IsAny(string key);
    ```

    Checks if there is any controller with given key.

---

- FindFirst

    ```csharp 
    TController FindFirst<TController>() where TController : Controller;
    ```

    Returns the first controller instance

---

- FindAll

    ```csharp
    List<TController> FindAll<TController>() where TController : Controller;
    ```

    Returns all controller instances of a specified type  

---

- TryGetKey

    ```csharp
    bool TryGetKey(Controller controller, out string key); 
    ```

    Tries to find the key of given controller

---

- GroupUnderKey

    ```csharp
    void GroupUnderKey(Controller controller, string key);
    ```

    Changes the controller key to another key if controller exists.

---

## Controller Details

Controllers are supposed to keep action definitions and they are used as a main component in a UserController.

**Initialize:** Initializes the controller

**AddHandlers:** Register your actions here

**HandleAction:** Registers an ActionHandler with a given name

**InvokeAction:** Invokes the action with a given parameters

**ResetController:** Resets the view back to the initial condition

**Open:** Opens the view

**Close:** Closes the view

![ControllerInspector](/img~/ControllerInspector.png)

## View Details

Views are all about the opening and closing a certain user control or element. They can be used alone, if methods are accessed someone of course, yet we encourage it to be used in MVC Framework as described in documentation.

**OnOpenBegin:** Event thrown when the Open animation begins

**OnOpenEnd:** Event thrown when the Open animation ends

**OnCloseBegin:** Event thrown when the Close animation begins

**OnCloseEnd:** Event thrown when the Close animation ends

(!) Subscribe to above events to program actions synchronized with animation.

**IsAnimating:** Returns if open or close animation is playing.

**IsOpen:** Returns if View fully open.

**ResetView:** Resets the view back to the initial condition

**Open:** Opens the view

**Close:** Closes the view

**Bind:** Add listeners for reflective properties here. This is model bound method.

**Unbind:** Remove listeners from reflective properties here. This is model bound method.

### View Types

#### Animator View

Here is an example of what inspector looks like with a view created by animator template.

![ViewAnimatorInspector](/img~/ViewAnimatorInspector.png)

* **Content Root**, **Background Blocker** and **Foreground Blocker** are explained in section *How to Create an Animated Popup (UserControl)*

* **Default Reset First:** If checked, it resets the animation everytime it opens the animation.

* **Open Trigger:** Trigger parameter which used to trigger change from Idle state to Open state.

* **Close Trigger:** Trigger parameter which used to trigger change from Open state to Close state.

* **Idle State Name:** State name of Idle state.

* **Animator Comp:** Reference to the animator component which is located on root.

  Observe that we designed it to have a specific Animator State Machine which can be used in this type of View.

    ![SampleAnimator](/img~/SampleAnimator.png)

  This animator can be arranged to have additional states to accommodate the game's needs. However, the basic state machine should follow the rules of; 

  * Transition from Idle to Open should be with "Open Trigger".
  * Transition from Open to Close should be with "Close Trigger".
  * After Close animation completed, it should go and stay at the "Idle" state.
  
  If animator template is used, we have a sample animation controller which we automatically reference. To create a sample animation controller of your own, you can use `Create Sample Animation `  which can be accessed in project window on `Create > Matchingham > MG Stockholm > Create View Animation` . Later on, you can easily adapt this controller for your game's needs, i.e. just change the animations referred by Open and Close states.
  
  **Important:** Animation controller provided by this API contains StateMachineBehaviour scripts attached to Open and Close states. Any other animation controllers without proper OpenBehaviour(.cs) and CloseBehaviour(.cs) components, would not be able to produce correct events.

#### Scaled Linear View

![ViewScaledLinear](/img~/ViewScaledLinear.png)

* Open command scales content root transform from Vector2.zero to Vector2.one in given "Duration".
* Close command scales content root transform from Vector2.one to Vector2.zero in same duration. In this example it is .5 seconds.
* If no root is defined, it uses the transform this component attached.

#### Scaled Animation Curve View

![ViewScaledCurve](/img~/ViewScaledCurve.png)

* This view additionally uses an Animation Curve component to decide how animation acts when opening up and closing down.
* In Animation Curve, you can define the scale in between [0, 1] time scale. The data outside the [0, 1] time interval is obsolete.
* Observe that this view still uses Duration component which is the total time it takes for animation.

#### Faded Linear View

![ViewFadedLinear](/img~/ViewFadedLinear.png)

* Open command fades content root image from transparent to opaque in given "Duration".
* Close command fades content root image from transparent to opaque in same duration. In this example it is .5 seconds.
* If no root is defined, it uses the transform this component attached.

#### Faded Animation Curve View

![ViewFadedCurve](/img~/ViewFadedCurve.png)

* This view additionally uses an Animation Curve component to decide how animation acts when opening up and closing down.
* In Animation Curve, you can define the scale in between [0, 1] time scale. The data outside the [0, 1] time interval is obsolete.
* Observe that this view still uses Duration component which is the total time it takes for animation.

#### Empty View

Empty view does not use any animation, and just enables and disables view in Open and Close.

## Model Details

Reflective properties related to model definition comes here. Also some localization functionality should be introduced in game cycle.

## Stockholm Configuration

In API, we included some string based configurations inside StockholmConfig file, which can be accessed through `Matchingham > UI Editor Config`.  It is in fact a scriptable object created inside Resources file, and it can be updated in inspector. We suggest these settings are not to be touched, since these are extensively used in our editor functions and it may not work properly, however these paths and naming rules can be easily updated here.

![StockholmConfig](/img~/StockholmConfig.png)

## Code Samples

Going back to our initial scenario, we are going to complete WinController. First of all we need to mount, unmount and open it via our UI Manager.

UIManager.cs

``` csharp
public class UiManager : MonoBehaviour
{
    [SerializeField] Button mountButton;	// To mount
    [SerializeField] Button unmountButton;	// To unmount
    [SerializeField] Button winButton;		// To Open mounted controller
	[SerializeField] Transform popupCanvas;	// To mount our popups on
	
    private WinController winInstance;

    private void Start()
    {
        AddHandlers();
    }

    private void AddHandlers()
    {
        mountButton.onClick.AddListener(MountWin);
        unmountButton.onClick.AddListener(UnmountAll);

        winButton.onClick.AddListener(() => 
        { 
            if(winInstance != null)
            {
                winInstance.Open();
            }
        });
    }

    private void MountWin()
    {
        if (winInstance != null)
        {
            return;
        }
        
        winInstance = popupCanvas.Mount<WinController>();
    }

    private void UnmountAll()
    {
        if (winInstance != null)
        {
            winInstance.Unmount();   
        }
    }
}
```

WinController.cs

``` csharp
namespace UI.Controllers
{ 
    public class WinController : Controller<WinView, WinModel>
    {
        public override void ResetController()
        {
            base.ResetController();
        }

        public override void Open()
        {
            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
            
            base.Open();
            AddRandomGold();
        }
		
        public override void Close()
        {
            base.Close();
        }
	
        protected override void Initialize()
        {
            base.Initialize();

            View.animImage.ResetView();
            View.animButton.ResetView();
        }

        protected override void AddHandlers()
        {
            // Define the functionality of buttons etc. here..
            View.OnOpenEnd += new View.OnOpenEndEvent(View.animImage.Open);				
            View.animImage.OnOpenBegin += new View.OnOpenBeginEvent(View.animButton.Open);
            View.animImage.OnOpenBegin += new View.OnOpenBeginEvent(View.animText.Open);

            View.animButton.GetComponent<Button>().onClick.AddListener(Close);
        }

        private void AddRandomGold()
        {
            Model.currentGold.Value += 10 * UnityEngine.Random.Range(0, 50);
        }
    }
}
```

WinView.cs

``` csharp 
namespace UI.Views
{ 
	public class WinView : AnimatedView<WinModel>
    {
        [SerializeField] internal View animImage;
        [SerializeField] internal View animButton;
        [SerializeField] internal View animText;
        [SerializeField] internal Text totalGoldText;

        private Text fadeText;

        private void Awake()
        {
            fadeText = animText.GetComponent<Text>();
        }

        // Create binding with your associated model
        private void CongratulationsBinding(bool init, int defaultVal)
        {
            fadeText.text = $"Congratulations on your\n{defaultVal} Coins";
        }

        private void TotalGoldBinding(bool init, int defaultVal)
        {
            totalGoldText.text = $"{defaultVal} $";
        }

        protected override void Bind()
        {
            // Attach your binding to Model here
			Model.currentGold.Subscribe(CongratulationsBinding);
            Model.currentGold.Subscribe(TotalGoldBinding);
            Debug.Log($"Bind called from {nameof(WinView)}");
        }

        protected override void Unbind()
        {
            // Detach your binding from Model here
			Model.currentGold.Unsubscribe(CongratulationsBinding);
            Model.currentGold.Unsubscribe(TotalGoldBinding);
            Debug.Log($"Unbind called from {nameof(WinView)}");
        }
    }
}
```

WinModel.cs

```csharp 
namespace UI.Models
{
    public class WinModel
    {
        // Create your properties here, in a bindable fashion
		public Subject<int> currentGold = new Subject<int>(100);
    }
}
```

Final UI With Referenced Components

![WinDemoFinal](/img~/WinDemoFinal.png)
