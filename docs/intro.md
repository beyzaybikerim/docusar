---
sidebar_position: 1
slug: /
---

# Matchingham SDK Introduction
## Getting Started

:::warning

Matchingham SDK works with C# 8 so to use the SDK you have to have latest versions of Unity (2020+).

:::

Get started by **Adding New Scopes**.
Head over this path and fill out the registries as shown in the image below

:::important Path
> Unity Editor > Project Settings > Package Manager > Scoped Registries
:::

![scoped-registiry](/img~/scoped-registiries.png)


or you can append this object to  `Packages/manifest.json` file.

```js
  "scopedRegistries": [
    {
      "name": "MG Package Registry",
      "url": "https://upm.matchingham.gs",
      "scopes": [
        "com.matchinghamgames",
        "com.google",
        "com.adjust",
        "com.admost",
        "com.azixmcaze",
        "com.jimmycushnie",
        "com.dbrizov"
      ]
    },
    {
      "name": "NPM",
      "url": "https://registry.npmjs.org",
      "scopes": [
        "com.onesignal"
      ]
    }
  ]
```

Now you will be able to access all packages inside Unity Package Manager.
Change packages to My Registries. 


![my-registries](/img~/my-registries.png)

<!--Now you will see the full list of packages. Click install button from any package you want to install.-->
<!---->
<!--![my-registries](/img/full-packages.png)-->

Now you will see the list of packages. If you haven't authenticated yourself you will only be able to see ` UPM Config ` package under Matchingham registeries. To see all available packages from Matchingham SDK, you should install that one package and move into next steps from here.

![unity-packagesupm](/img~/unity-packagesupm.png)

## Upm Authentication

:::note

Before you move to the next step, please contact with Matchingham SDK admin and provide them with your GitLab username.

:::

Firstly you have to create an access token from your gitlab account. To do so, you have to sign in to your gitlab account and go to preferences from dropdown menu by clicking on your profile image on top right.

![gitlab-prefs](/img~/gitlab-prefs.png)

Then you have to click on the Access Tokens menu on the left. Here you can create an access token by giving it a name. You have to have read user option selected before creating your token. After that an access token will appear on the top of the screen when you click create. Then you have to copy that token into your clipboard and save it on your computer because you won't be able to get it again.

![gitlab-access](/img~/gitlab-access.png)

After getting the access token, move back to the Unity and from top bar go to Matchingham -> UpmConfig. This will open up a window where you can put your gitlab username and access token. After filling in those fields, you can now click on Authenticate button to be authenticated. Following this step, another window will appear with authentication result.

![unity-upmwindow](/img~/unity-upmwindow.png)

:::info

Once you have completed the authentication steps, you can remove the package from your project. You will be automatically authenticated for the SDK usages in the future on the same device.

:::

 Now you should be able to see all the packages from Matchingham SDK. If you aren't seeing the full list, try refreshing the packages list or restart Unity. You can start installing packages from package manager.
 
:::tip My tip

If you get any errors about package dependency after installation, please try updating the package to the latest version using package manager. This will resolve any issues that may rise.

:::

![my-registries](/img~/full-packages.png)

After adding the appropriate packages into your project, you would need to add an initializer script that calls all the required initializer methods of each SDK that you integrated. To do so, you can make use of a script like below.

```csharp
    /// <summary>
    /// This method must be called within Awake method in order to initialize
    /// all modules at an earliest time
    /// </summary>
    private void InitializeMGSDK()
    {
        PlayerDataManager.Instance.InitializeWithPlayerPrefs(); // Init Stash Module
        PlayerDataManager.Instance.WhenReady(_ => // Set callback method for when the module is ready
        {
            Vegas.Instance.Initialize(); // Init Vegas Mediation Module
        
            Meteor.Instance.Initialize(); // Init Meteor Remote Config Module
            Meteor.Instance.WhenReady(_ => // Set callback method for when the module is ready
            {
                /*
                 * It is important to initialize Meteor module well before the other SDK
                 * initializations in order to have remote config settings in hand
                 * to be used in other modules (e.g. you might need remote config values
                 * to set notifications with right title and message with Charlie module
                 */
                Charlie.Instance.Initialize(); // Init Charlie Notification Manager Module
                Dealer.Instance.Initialize(); // Init Dealer In-app Purchase Module
                Court.Instance.Initialize(); // Init Court Rating/Review Module
                Enforcer.Instance.Initialize(); // Init Enforcer Force Update Module
                Enforcer.Instance.WhenReady(_ => // Set callback method for when the module is ready
                {
                    /*
                     * DataUsageConsentManager calls its initialize method itself so 
                     * we just ask for user consent after successful initialization
                     * otherwise carry on with other module initializations
                     */
                    DataUsageConsentManager.Instance.WhenReady(initState =>
                    {
                        if (initState == InitializationResult.Initialized)
                        {
                            DataUsageConsentManager.Instance.RequestUserConsent(AfterConsent);
                        }
                        else
                        {
                            AfterConsent();
                        }
                    });
                });
            });
        });
        
        void AfterConsent()
        {
            Promoter.Instance.Initialize(); // Init Cross Promotion Module
            PrivacyPolicy.Instance.Initialize(); // Init Privacy Policy Module
            Sherlock.Instance.Initialize(); // Init Sherlock Analytics Module
            Sherlock.Instance.WhenReady(_ =>
            {
                // call your methods to start game here
            });
        }
    }
```
