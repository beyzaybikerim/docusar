# Pool Manager

Pools objects to be re used.

## How to use

* Convert your `Instantiate` calls to `PooManager.Instance.Spawn`. Both methods have the same signatures and overloads, so 
simply updating the method is going to be enough.
* Convert your `Destroy` calls to `PoolManager.Recycle`. This will return instances that are no longer needed back to the
pool.
* Go to your game manager, and call `PoolManager.Create()` method when the game is being initialized.
  * You don't really need to do this if you are using the extension methods.
* If you want, you can use the extensions methods.
  * For any `UnityEngine.Object` you instantiate, you can use the `Spawn` and `Recycle` extensions. Sample:
    * `bulletExplosion.Spawn();`
    * `bulletExplosion.Spawn(hit.point, Quaternion.LookRotation(hit.normal));`
    * `bulletExplosion.Recycle();`
  * When using extension methods, you don't really need to call `PoolManager.Create()` as extension methods internally
  do this already.

## Quick Tips

* `PoolManager` can keep different pools of the same type. Because it is `object` based.
  * This means, if you have a base `Projectile` class that is used in 10 different projectile prefabs, all you need to
  do is to spawn each one normally, pool manager should create a new (not spawned before) pool for each new prefab you 
  try to `Spawn`.
  

* Extension methods internally check `PoolManager` instance and create one if it doesn't exist. But normal API doesn't
do this. The reason for this is, the extensions are shorthands to make it easier to use the system. However, if you want
to use the PoolManager in a custom way, you directly interact with `PoolManager` to achieve your desired result.
  * One example this could be useful for is:
    * You have a game that is consisted of multiple different game modes.
    * Each game mode uses a different prefabs, so the prefabs of the previous game modes are not necessary.
    * You can destroy the `PoolManager` game object when a game mode unloads, and create a new one when a new game mode
    starts.


## API

```csharp
public void PoolManager:Create()
```

Creates an instance of `PoolManager`.


```csharp
public T PoolManager:Spawn<T>(T prefab) where T : Object
public T PoolManager:Spawn<T>(T prefab, Transform parent) where T : Object
public T PoolManager:Spawn<T>(T prefab, Vector3 position, Quaternion rotation) where T : Object
public T PoolManager:Spawn<T>(T prefab, Vector3 position, Quaternion rotation, Transform parent) where T : Object
```

Spawns a pooled instance of provided prefab, cached as its reference type. If no objects are available in the pool, creates
a new object.


```csharp
public void PoolManager:Recycle<T>(T instance) where T : Object
```

Recycles an object instance spawned by the `PoolManager`. The objects that are not created by `PoolManager` cannot be
recycled.

### Extensions

```csharp
public static T Spawn<T>(this T prefab) where T : Object
public static T Spawn<T>(this T prefab, Transform parent) where T : Object
public static T Spawn<T>(this T prefab, Vector3 position, Quaternion rotation) where T : Object
public static T Spawn<T>(this T prefab, Vector3 position, Quaternion rotation, Transform parent) where T : Object
```

Works the same as `PoolManager:Spawn<T>(..)` however also creates a `PoolManager` instance if necessary


```csharp
public static void Recycle<T>(this T instance) where T : Object
```

Works the same as `PoolManager:Recycle<T>(T)` however also creates a `PoolManager` instance if necessary
