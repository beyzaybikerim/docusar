# Daily Rewards

Universal system for providing rewards every 24 hours or via ads.

## How to Use

* Open `Matchingham > Daily Rewards Config`.
* Make sure `Enabled` checkbox is checked.
* Add rewards to pool in the config inspector.
    * Thumbnails are added via key mapping, as embedding them into reward data
      makes the data class incompatible with json serialization. This prevents
      remote configuration.
* Subscribe to `DailyRewardManager.Instance.CurrentReward` to get current reward and
  define what happens when the reward changes.
* Subscribe to `DailyRewardManager.Instance.DailyRewardAvailable` to get current reward availability
  state, and define what happens when availability changes.
    * You can update the collect button on daily reward ui to become a rewarded video button when
      availability becomes false, for example.
* Use `DailyRewardManager.Instance.ClaimReward()` to claim current reward.
    * Please note that if you are not using backpack module, you need to manually add those items
      to player's inventory. You still need to call `DailyRewardManager.Instance.ClaimReward()`
      method though.
* You can get a range of random rewards (for history and upcoming rewards) using the 
  `DailyRewardManager.Instance.GetRewards(int, int)` API.

## API & Details

### Daily Reward Manager



* **DailyRewardAvailable**: If daily reward is available, this value becomes `true`.
    * You can use this value to toggle free/rewarded claim methods.



* **CurrentReward**: Gets current reward info. Contains reward item id, quantity and thumbnail sprite.



* **TimeToNextReward**: Returns a time span indicating the time remaining to next reward.
    * You can use this to create a timer display on Daily Rewards UI.



* **RollCount**: Number of times the reward was collected. Also the current reward index.
    * If you want an upcoming rewards and previous rewards feature for example, you can use the following code:
    ```c#
    int rollIndex = DailyRewardManager.Instance.RollCount;
    DailyRewardManager.Instance.GetRewards(rollIndex - 5, rollIndex + 5);
    ```



* **ClaimReward()**: Call when user claims the reward.



* **WhenInitialized(Action)**: Allows you to register a callback that will be fired only
  after the module is successfully initialized. Use this to execute logic that requires
  this module to be initialized first. If the module has already initialized, immediately
  invokes the callback



* **WhenFailedToInitialize(Action)**: Allows you to register a callback that will be fired only after
  the module fails to initialize for any reason. Use this to handle what should happen
  in case this module fails to initialize. If the module has already failed to initialize, immediately
  invokes the callback.



* **WhenReady(Action)**: Combined version of `WhenInitialized` and `WhenFailedToInitialize`.
  Delays execution of callback till module is first initialized or failed to initialize, immediately invoke
  the callback if it is already initialized or failed to initialize.
