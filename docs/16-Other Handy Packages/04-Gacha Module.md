# Gacha Module

Provides simple solution for rolling gachas.

## How to Use

* Import the package from Package Manager UI
* Create `Gacha Pool` object anywhere in the project's `Assets` folder.
  
  ![Creating Gacha Pool](/img~/creating-gacha-pool.png)

* Select the `Gacha Pool` object you created and populate it. Make sure probabilities add up to 100.
    * You can set the probability for the most important entries and let the editor 
      distribute remaining probabilities automatically.
* Open config from `Matchingham > Gacha > Config` and add your gacha pool pool to the dictionary.
    * Add the first one as "default", as you can access this without even providing a parameter when rolling.

    ![Adding pools to config](/img~/gach-menu.png)
  
    ![Adding pools to config](/img~/adding-new-gacha-pool.png)

* Call `GachaManager.Instance.Roll()` or `GachaManager.Instance.Roll(poolId: string)` anywhere in your code
to get a random gacha reward from the pool.
* Make sure to call `GachaManager.Instance.CollectReward(reward: GachaReward)`
to add rewarded item's to player's inventory.
    * If you are not using Backpack Module, you need to modify this method to work with your own system!
      

## Integrating a Custom Item System (Not Using Backpack)

You might be integrating the Gacha Module into an already published game, that has its own item management system. Migrating the
entire item/inventory system of the game to Backpack might be undesired in such a case. In that case, you can implement your 
own way of integrating the Gacha Module to your current item system.This is very simple.

Let's say your game has a not very generic item system that only supports unlockable items, which are persisted between sessions 
via flags set in the `PlayerPrefs`. We can integrate Gacha Module into this game by creating a simple class that will define
which item flag/flags should be set and giving those objects as gacha rewards. 

First, since you are not using Backpack module, you will get compile errors. These are because Gacha System uses Backpack module by default and thus calls Backpack APIs. You will
need to remove these.

Then, create an `Item` class that contains serialized fields for each one of your items, preferably under `Assets/MatchinghamAssets/GachaSystem/Scripts/Models` folder.

```csharp
[System.Serializable]
public class Item
{
    public bool cosmeticItemA;
    public bool cosmeticItemB;
    public bool cosmeticItemC;
    public bool cosmeticItemD;
    
    public bool weaponA;
    public bool weaponB;
    public bool weaponC;
    public bool weaponD;
}
```

Then go to `Assets/MatchinghamAssets/GachaSystem/Scripts/Managers/GachaManager.cs` and change the `CollectReward(reward: GachaReward)` method to set related flags when called. In this example
we assume player data is kept in the `PlayerPrefs` and data keys are the flag names.

```csharp
public void CollectReward(GachaReward reward)
{
    if (reward.reward.cosmeticItemA) PlayerPrefs.SetInt("cosmeticItemA", 1);
    if (reward.reward.cosmeticItemB) PlayerPrefs.SetInt("cosmeticItemB", 1);
    if (reward.reward.cosmeticItemC) PlayerPrefs.SetInt("cosmeticItemC", 1);
    if (reward.reward.cosmeticItemD) PlayerPrefs.SetInt("cosmeticItemD", 1);
    
    if (reward.reward.weaponA) PlayerPrefs.SetInt("weaponA", 1);
    if (reward.reward.weaponB) PlayerPrefs.SetInt("weaponB", 1);
    if (reward.reward.weaponC) PlayerPrefs.SetInt("weaponC", 1);
    if (reward.reward.weaponD) PlayerPrefs.SetInt("weaponD", 1);
}
```

Now open Gacha System configuration, create a pool and start defining gacha reward entries there.

When you call `GachaManager.Intance.Roll()` or `GachaManager.Instance.Roll(poolId: string)`, you will receive a `GachaReward` object as the return value, which you will
need to pass into the `GachaManager.Instance.CollectReward(reward: GachaReward)` method.

* NOTE! Since this example uses only singular unlockable items as an example, we totally ignored the `Quantity` value.

## API & Details

### Gacha Manager

* **GachaReward Roll(poolId: string = "default")**: Rolls a random item from the pool identified by the provided id.


* **GachaReward Roll(pool: GachaPool)**: Rolls a random item from the pool.


* **CollectReward(gachaReward: GachaReward)**: Call this method to add the rewarded items to player's inventory.
