# Changelog

## [2.0.5] - 10-03-2022
### Fixed
* Old meteor dependency removed

## [2.0.4] - 09-03-2022
### Changed
* Root namespace on asmdef files

## [2.0.3] - 09-03-2022
### Changed
* Package name

## [2.0.2] - 09-03-2022
* Initial release