# Changelog
All notable changes to this project will be documented in this file.

* This package uses DD-MM-YYYY date format, noted for clarity

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.4.3
* Added detailed log calls

## 1.4.4
* Added log flags config and exposed it as module log flags from Sherlock for service use