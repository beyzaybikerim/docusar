# Changelog

## [1.7.6] - 09-03-2022
### Updated
* Namespaces on asmdef files

## [1.7.5] - 08-03-2022
* Initial release